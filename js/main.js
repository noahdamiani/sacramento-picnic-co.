simpleCart({
	checkout: {
		type: "PayPal",
		email: "billing@sacpicnics.com"
	},
 	cartStyle : "div"
	
	});
	
	simpleCart.bind( "afterAdd" , function( item ){
	 	$(".item-added").html( item.get("name") + " was added to the cart!" );
	 	$(".item-added").fadeIn('slow').delay(2000).fadeOut('slow');
	});
	simpleCart.bind( "beforeRemove" , function ( item ){
		 $(".item-added").html( item.get("name") + " was removed from the cart!" );
	});
	$(".item_add").click(function(){
		if ($(".shopping-cart").is(":hidden")) {
			$(".shopping-cart").slideToggle('fast');
		}
	});
	$(".fa-caret-down").click(function(){
		$(".shopping-cart").slideToggle('fast');
	});
	$(".close-cart").click(function(){
		$(".shopping-cart").slideToggle('fast');
	});
	$(".shopping-cart").append('<div class="cart-total"><a href="javascript:;" class="simpleCart_empty"> Empty Cart </a><span>or </span><a href="javascript:;" class="simpleCart_checkout"> Checkout</a></div>');
	
	
	
/*	(function($) {
    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
	}(jQuery));
	
	$('.showcustom').clickToggle(function() {
		$(".basket-slideout").slideToggle();
	     var customBasket = simpleCart.add({ 
				    name: "Custom Basket" ,
				    price: 25.00 ,
				    quantity: 1
				});
	}, function() {
		$(".basket-slideout").slideToggle();
	    var customBasket = simpleCart.add({ 
				    name: "Custom Basket" ,
				    price: 25.00 ,
				    quantity: 1
				});
			customBasket.remove();

	});
	var customBasket = simpleCart.add({ 
				    name: "Custom Basket" ,
				    price: 25.00 ,
				    quantity: 1
				});
			customBasket.remove(function(){
				simpleCart.empty();
			});

	
});*/